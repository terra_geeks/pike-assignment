locals {
  name            = "wittylight-${replace(basename(var.component_name), "-", "-")}"
}

# sg for database
resource "aws_security_group" "mysql_sg" {
  name        = "mysql_sg"
  description = "allow witty_app"
  vpc_id      = local.vpc_id

  ingress {
    description     = "allow witty_app"
    from_port       = 3306
    to_port         = 3306
    protocol        = "tcp"
    security_groups = [aws_security_group.witty_app.id]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "mysql_sg"
  }
}


module "aurora" {
 source = "git::https://github.com/Bkoji1150/aws-rdscluster-kojitechs-tf.git?ref=v1.1.11"

component_name = var.component_name
  name           = local.name
  engine         = "aurora-mysql"
  engine_version = "5.7.mysql_aurora.2.10.1"
  instances = {
    1 = {
      instance_class      = "db.r5.large"
      publicly_accessible = true
    }
  }

  vpc_id                 = local.vpc_id
  db_subnet_group_name   = module.vpc.database_subnet_group_name

  create_db_subnet_group = false

  iam_database_authentication_enabled = true
  apply_immediately   = true
  skip_final_snapshot = true
  
   vpc_security_group_ids = [aws_security_group.mysql_sg.id]

  enabled_cloudwatch_logs_exports = ["audit", "error", "general", "slowquery"]
  database_name                   = "postgres_aurora"
  master_username                 = var.master_username
}
  
