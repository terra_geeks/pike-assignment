locals {
  mandatory_tag = {
    line_of_business        = ""
    ado                     = "max"
    tier                    = "WEB"
    operational_environment = upper(terraform.workspace)
    tech_poc_primary        = "workhard2playhard10@gmail.com"
    tech_poc_secondary      = "workhard2playhard10@gmail.com"
    application             = "http"
    builder                 = "workhard2playhard10@gmail.com"
    application_owner       = "wittylight.com"
    vpc                     = "WEB"
    cell_name               = "WEB"
    component_name          = var.component_name
  }
}