
data "aws_availability_zones" "available" {
  state = "available"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "wittylight-vpc"
  cidr = "10.0.0.0/16"

  azs             =  data.aws_availability_zones.available.names
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
  database_subnets =  ["10.0.21.0/24", "10.0.23.0/24", "10.0.25.0/24"]

  enable_nat_gateway = true
  enable_vpn_gateway = true

}


locals {
  vpc_id               = module.vpc.vpc_id
  pub_subnet           =  module.vpc.public_subnets
  pri_subnet           =  module.vpc.private_subnets
  private_sunbet_cidrs =  module.vpc.private_subnets_cidr_blocks
  database_subnet      =  module.vpc.database_subnets
  instance_profile = aws_iam_instance_profile.instance_profile.name
  mysql            = data.aws_secretsmanager_secret_version.rds_secret_target
  instances = {
    "app1" = {
      instance_type = "t2.xlarge"
      subnet_id     = local.pri_subnet[0]
      user_data     = file("${path.module}/template/frontend_app1.sh")
    }
    "app2" = {
      instance_type = "t2.xlarge"
      subnet_id     = local.pri_subnet[1]
      user_data     = file("${path.module}/template/frontend_app2.sh")
    }
  }
}

data "aws_secretsmanager_secret_version" "rds_secret_target" {

  depends_on = [module.aurora]
  secret_id  = module.aurora.secrets_version
}

resource "aws_instance" "frond_end" {
  for_each = {
    for id, instances in local.instances : id => instances
  }
  ami                    = data.aws_ami.ami.id
  instance_type          = lookup(each.value, "instance_type")
  subnet_id              = lookup(each.value, "subnet_id")
  vpc_security_group_ids = [aws_security_group.front_app_sg.id]
  user_data              = lookup(each.value, "user_data")
  iam_instance_profile   = local.instance_profile

  tags = {
    Name = each.key
  }
}

resource "aws_instance" "witty_app" {
  depends_on = [module.aurora]
  count      = length(var.name)

  ami                    = data.aws_ami.ami.id
  instance_type          = "t2.xlarge"
  subnet_id              = element(local.pri_subnet, count.index)
  iam_instance_profile   = local.instance_profile
  vpc_security_group_ids = [aws_security_group.witty_app.id]
  user_data = templatefile("${path.root}/template/witty_app.tmpl",
    {
      endpoint    = jsondecode(local.mysql.secret_string)["endpoint"]
      port        = jsondecode(local.mysql.secret_string)["port"]
      db_name     = jsondecode(local.mysql.secret_string)["dbname"]
      db_user     = jsondecode(local.mysql.secret_string)["username"]
      db_password = jsondecode(local.mysql.secret_string)["password"]
    }
  )
  tags = {
    Name = var.name[count.index]
  }
}
