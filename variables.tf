

variable "region" {
  type    = string
  default = "us-east-1"
}

variable "component_name" {
  default = "wittylight"
}

variable "name" {
  type    = list(any)
  default = ["witty_app1", "witty_app2"]
}

variable "dns_name" {
  type = string
}

variable "subject_alternative_names" {
  type    = list(any)
}


variable "master_username" {
  description = "Username for the master DB user"
  type        = string
}

variable "database_name" {
  description = "Name for an automatically created database on cluster creation"
  type        = string
}
